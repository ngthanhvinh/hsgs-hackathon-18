import React from "react";
import { storiesOf } from "@storybook/react";
import { number, withKnobs } from "@storybook/addon-knobs";
import ReactGame from "react-gameboard/lib/component";
import Paragraph from "../src/N09/index.jsx";
import Game from "../src/N09/lib/N09.js";

const N09 = ReactGame(Game);

storiesOf("N09", module)
  .addDecorator(withKnobs)
  .add("N09 Game", () => (
    <N09 N={5}>
      <Paragraph />
    </N09>
  ))
  .add("Custom", () => {
    const options = {
      min: 1,
      max: 10,
      step: 1,
      range: true
    };
    return (
      <N09 N={number("Number of operations", 5, options)}>
        <Paragraph />
      </N09>
    );
  });
